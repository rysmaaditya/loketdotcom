# Loket.com Mini Project Submission

This is a mini project submission from Loket.com. In this project, what I've made is create database schedule to accomodate storing event data and export functionalities through HTTP API endpoint.

## Getting Started
In this assignment submission, I built this project using Laravel v5.7 PHP framework. Every development steps documented in [assumption.md](assumption.md).

## Installing

1. Run git clone https://rysmaaditya@bitbucket.org/rysmaaditya/loketdotcom.git
2. Configure `.env file`
3. Configure permission on laravel if needed. On Linux environment, you can use [this](https://stackoverflow.com/questions/30639174/file-permissions-for-laravel-5-and-others)
4. Run migration to generate database scheme and seeder `php artisan migrate:fresh --seed`
5. API endpoint is ready to use

## Running the tests

API endpoint functionalities, path, method and parameters, defined as below. These endpoint based on the test given.

| Endpoint           | Relative Path             |  Method  | Parameters                                                               |
| ------------------ |:--------------------------|:--------:| :------------------------------------------------------------------------|
| Create Event	     | /api/event/create         |   POST   | `name:string`, `location_id:int`, `start_at:datetime`, `end_at:datetime` |
| Create Location    | /api/location/create	     |   POST   | `name:string`                                                            |
| Create Ticket      | /api/event/ticket/create  |   POST   | `name:string`, `event_id:int`, `price:int`, `quota:int`                  |
| Get Event          | /api/event/get_info       |   GET    | `id:int`                                                                 |
| Purchase Ticket    | /api/transaction/purchase |	 POST   | `user_id:int`, `ticket_type_id[]:string`, `quantity[]:string`            |
| Transaction Detail | /api/transaction/get_info |   GET    | `id:int`                                                                 |