# Loket.com Development Process Explanation

In this section, there are several things to explain about development process. Those process are
1. Entity relationship diagram design
2. Making DB migration and model
3. Analyzing constraints from requirements mentioned
4. Create web service JSON API with defined functionalities
5. Recap and analysis

## ERD Design

![alt text](https://bitbucket.org/rysmaaditya/loketdotcom/raw/7e840238c498d73f8d9165772341889a12c9f411/public/Loketdotcom.png "Entity Relationship Diagram")

As you can see ERD design above, here are the explanations how this ERD made.
1. One event will have exactly one location (event with location has one to one relationship)
2. One location might have one or more event (location with event has one to many relationship)
3. One event will have exactly one schedule (I made the schedule as attribute in event entity rather than made it another entity)
4. One schedule might start and end at different day (the attribute that i made containted of `start_at` and `end_at`)
5. One event might have more than one ticket type (event with tickettype has one to many relationship)
6. Customer will be able to make transaction to purchase ticket event many times (customer entity defined as user has one to many relationship with transaction)
7. For one transaction, customer could buy 1 or more ticket type (transaction has one to many relationship with transaction details, transaction details, has one to one relationship with ticket type, transaction details representing each ticket type and quantity that customer bought)

## Making DB migration and model

Based on ERD design that explained earlier, the next step is to define what entity attribute should be needed to create table and their columns later that will be used in future step of development.

- Event

    In this event entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `name` [string][required]
      * `location_id` [integer][required][foreign]
      * `start_at` [datetime][required]
      * `end_at` [datetime][required]
      * `created_at` [timestamp]
      * `updated_at` [timestamp]

- Location

    In this location entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `name` [string][required]
      * `created_at` [timestamp]
      * `updated_at` [timestamp]

- Ticket Type

    In this ticket type entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `name` [string][required]
      * `event_id` [integer][required][foreign]
      * `price` [double][required]
      * `quota` [integer][required]
      * `created_at` [timestamp]
      * `updated_at` [timestamp]

- Transaction

    In this transaction entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `user_id` [string][required][foreign]
      * `total_price` [double][required]
      * `created_at` [timestamp]
      * `updated_at` [timestamp]

- Transaction Details

    In this transaction details entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `transaction_id` [integer][required][foreign]
      * `ticket_type_id` [integer][required][foreign]
      * `quantity` [integer][required]
      * `subtotal` [double][required]
      * `created_at` [timestamp]
      * `updated_at` [timestamp]

- User

    In this user entity, there are some attributes that contained in it
    
      * `id` [integer][primary][incremental]
      * `name` [string][required]
      * `email` [string][required]
      * `email_verified_at` [timestamp]
      * `password` [string][required]
      * [rememberToken]
      * [timestamps]

## Analyzing constraints from requirements mentioned

There are several constraints for each API endpoint function that must be considered before go to development phase.

### POST `/api/event/create`
- Endpoint to create new event
- One event will have exactly one schedule
- One schedule might start and end at different day
- Function that made must have value checking at start time and end time of the event
- Start time cannot be the day in the past. So the function must have today as input checking parameters to compare start time input
- End time of the event must be greater that start time event

### POST `/api/location/create`

- Endpoint to create new location
- One location might have one or more event

### POST `/api/event/ticket/create`

- Endpoint to create new ticket type on one specific event
- One event might have more than one ticket type
- One ticket type must have price and quota

### GET `/api/event/get_info/{id}`

- Endpoint to retrieve event information, including location data and ticket data
- The function must return location, ticket data, and event itself based on event id reference

### POST `/api/transaction/purchase`

- Endpoint to make a new purchase, customer data is sent via this API
- Customer will be able to make transaction to purchase ticket event
- Customer can purchase ticket event many times
- But, if quota is less than 1 then the ticket cannot be purchased
- For each transaction, customer only can purchase ticket within same event
- Within one transaction, customer can purchase more than 1 ticket type, and more than 1 qty per ticket type

### GET `/api/transaction/get_info/{id}`

- Endpoint to retrieve transaction created using endpoint purchase ticket
- The function must return transaction and transaction details info based on id transaction reference

## Web Service API Based on Endpoint

All request parameters in `form-data` format

### POST `/api/location/create`

params : `name` [string]

- create with correct params

   - `name:Tunjungan Plaza`
   - result : `{
    "status": 201,
    "message": "Location has been created successfully"
  }`

- create with empty name params
   - `name:`
   - result : `{
      "name": [
          "The name field is required."
      ]
  }`

### POST `/api/event/create`

params : `name` [string], `location_id` [integer], `start_at` [datetime], `end_at` [datetime]

- create with correct params
   - `name:Information Systems Expo`
   - `location_id:1`
   - `start_at:2019-09-24 20:00:00`
   - `end_at:2019-09-25 20:00:00`
   - result : `{
      "status": 201,
      "message": "Event has been created successfully"
  }`

- create with wrong location
   - `name:Information Systems Expo`
   - `location_id:100`
   - `start_at:2019-09-24 20:00:00`
   - `end_at:2019-09-25 20:00:00`
   - result : `{
      "status": 404,
      "message": "Location not found"
  }`

- create with start date before today
   - `name:Information Systems Expo`
   - `location_id:100`
   - `start_at:2017-09-24 20:00:00`
   - `end_at:2019-09-25 20:00:00`
   - result : `{
      "start_at": [
          "The start at must be a date after 09/23/2018 07:09:16 am."
      ]
  }`

- create with end date before start date
   - `name:Information Systems Expo`
   - `location_id:100`
   - `start_at:2019-09-24 20:00:00`
   - `end_at:2019-09-21 20:00:00`
   - result : `{
        "end_at": [
            "The end at must be a date after start at."
        ]
    }`

### POST `/api/event/ticket/create`

params : `name` [string], `event_id` [integer], `price` [integer], `quota` [integer]

- create with correct params

   - `name:Information Systems Expo Golden Ticket`
   - `event_id:2`
   - `price:100000`
   - `quota:200`
   - result : `{
      "status": 201,
      "message": "Ticket has been created successfully"
  }`

- create incorrect `event_id`

   - `name:Information Systems Expo Golden Ticket`
   - `event_id:20`
   - `price:100000`
   - `quota:200`
   - result : `{
      "status": 404,
      "message": "Event not found"
  }`

### GET `/api/event/get_info	`

params : `id` [integer]

- get event info if exist

   - URL /api/event/get_info/1
   - result : `{
    "id": 1,
    "name": "Jatim Fair Expo",
    "location_id": 1,
    "start_at": "2018-10-20 20:00:00",
    "end_at": "2018-10-21 20:00:00",
    "created_at": null,
    "updated_at": null,
    "tickettypes": [
        {
            "id": 1,
            "name": "Jatim Expo Golden Ticket",
            "event_id": 1,
            "price": 100000,
            "quota": 480,
            "created_at": null,
            "updated_at": "2018-09-22 21:21:42"
        },
        {
            "id": 2,
            "name": "Jatim Expo Silver Ticket",
            "event_id": 1,
            "price": 50000,
            "quota": 1300,
            "created_at": null,
            "updated_at": "2018-09-22 21:21:42"
        }
    ]
}`

- get event info if not exist

   - URL /api/event/get_info/100
   - result : `{
      "status": 404,
      "message": "Event not found"
  }`

### POST `/api/transaction/purchase	`

params : `user_id` [int], `ticket_type_id[]` [string], `quantity` [string]

- create with correct params

   - `user_id: 1`
   - `ticket_type_id[]: 2`
   - `quantity[]: 2`
   - `ticket_type_id[]: 1`
   - `quantity[]: 2`
   - result : `{
      "status": 201,
      "message": "Transaction has been created successfully"
  }`

- create with double `ticket_type_id` in one transaction

   - `user_id: 1`
   - `ticket_type_id[]: 2`
   - `quantity[]: 2`
   - `ticket_type_id[]: 1`
   - `quantity[]: 2`
   - `ticket_type_id[]: 1`
   - `quantity[]: 2`
   - result : `{"status":500,"message":"Failed to purchase ticket. Duplicate ticket types are not allowed"}`

   this is another constraint that should be considered. Because there should be no ticket with the same type in the same cart. So, i use `array_diff` for checking if there is any duplicate value of `ticket_type_id` in a transaction

- create with not exist `ticket_type_id`

   - `user_id: 1`
   - `ticket_type_id[]: 2`
   - `quantity[]: 2`
   - `ticket_type_id[]: 10`
   - `quantity[]: 2`
   - result : `{"status":404,"message":"Ticket not found"}`

   this is another constraint that should be considered. That `ticket_type_id` must be exist in the database to get referenced by transaction. If there is no `ticket_type_id` that matched as requested, the transaction must be fail

- create with `ticket_type_id` in a transaction not in the same event

   - `user_id: 1`
   - `ticket_type_id[]: 2`
   - `quantity[]: 2`
   - `ticket_type_id[]: 3`
   - `quantity[]: 2`
   - result : `{"status":500,"message":"Failed to purchase ticket. Cannot purchase ticket from different event"}`

   this is another constraint that should be considered. That `ticket_type_id` in transaction must be referencing the same `event_id` in a transaction

- create with `ticket_type_id` quota that less than customer request

   - `user_id: 1`
   - `ticket_type_id[]: 2`
   - `quantity[]: 2000`
   - `ticket_type_id[]: 1`
   - `quantity[]: 2000`
   - result : `{"status":500,"message":"Failed to purchase ticket. Ticket Jatim Expo Golden Ticket exceeds the available capacity"}`

   this is another constraint that should be considered. If `ticket_type_id` quota less that `quantity` requested, the transaction should fail

### GET `/api/transaction/purchase/{id}`

params : `id` [integer]

- get transaction info if exist

   - URL /api/transaction/get_info/1
   - result : `{
    "id": 1,
    "user_id": 1,
    "total_price": 3000000,
    "created_at": "2018-09-23 02:18:32",
    "updated_at": "2018-09-23 02:18:32",
    "transactiondetail": [
        {
            "id": 1,
            "transaction_id": 1,
            "ticket_type_id": 2,
            "quantity": 20,
            "subtotal": 1000000,
            "created_at": "2018-09-23 02:18:32",
            "updated_at": "2018-09-23 02:18:32"
        },
        {
            "id": 2,
            "transaction_id": 1,
            "ticket_type_id": 1,
            "quantity": 20,
            "subtotal": 2000000,
            "created_at": "2018-09-23 02:18:32",
            "updated_at": "2018-09-23 02:18:32"
        }
    ]
}`

- get event info if not exist

   - URL /api/transaction/get_info/100
   - result : `{
      "status": 404,
      "message": "Transaction not found"
  }`